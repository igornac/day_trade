import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import time
from copy import deepcopy

def cost_function(H, Y):
    """
    Retroune le coût du réseau pour un seul exemple
    :param real_output: le vecteur de sortie prédit par le réseau
    :param expected_output: le vecteur qui aurait du être retourné
    :return: le coût du réseau pour l'exemple
    """
    
    return np.mean(np.sum((H-Y)**2,axis=1))/2


def cost_derivative(H, Y):
    """
    Retourne la dérivé de la fonction de coût utilisée pour l'entrainement
    :param real_output: le vecteur de sortie prédit par le réseau
    :param expected_output: le vecteur qui aurait du être retourné
    :return: la dérivée du coût pour l'exemple
    """
    return H - Y


def sigmoid(x):
    """
    Retourne sig(x), qui correspond ici à la fonction d'activation des neurones du réseau
    :param x: la valeur de la somme des entrées du neurone pondérée par les poids avec le bias
    :return: la valeur de sortie du neurone
    """
    
    return 1.0 / (1.0 + np.exp(-x))


def sigmoid_derivative(x):
    """
    Retourne la dérivée de sig prise en x
    :param x: la valeur de la somme des entrées du neurone pondérée par les poids avec le bias
    :return: la dérivée de sig(x)
    """

    # Une des propriétés de la sigmoide donne sig'(x) = sig(x) * (1 - sig(x))    
    sig = sigmoid(x)

    return sig * (1 - sig)
class MultiPerceptron():
    
    def __init__(self,input_size,output_size,hidden_structure=()):
        
        self.layers = [input_size] + list(hidden_structure) + [output_size]
        L = len(self.layers)
        self.bias = [np.zeros(self.layers[i+1]) for i in range(L-1)]
        self.weights = [2*np.random.random((self.layers[i],self.layers[i+1]))-1 for i in range(L-1)]

    def prediction(self, X):

        X = self.normalize_input(X)
        L = len(self.layers)
        A = X
        for i in range(L-1):
          B = self.bias[i]
          W = self.weights[i]
          Z = np.dot(A,W) + B #numpy comprend
          A = sigmoid(Z)
        
        return self.denormalize_output(A)
      
    def backpropagation(self, X, Y):
      
        L = len(self.layers)
        delta_W, delta_B = [], []
        
        # Propagation avant avec mémoire
        A = X
        activations = [X]
        sums = []
        for i in range(L-1):
            B = self.bias[i]
            W = self.weights[i]
            Z = np.dot(A,W) + B #numpy comprends
            sums.append(Z)
            A = sigmoid(Z)
            activations.append(A)
            
        # Propagation arrière
        delta = cost_derivative(A,Y)*sigmoid_derivative(Z)
        dB = delta
        dW = np.einsum('bi,bj->bij',activations[-2],delta)
        
        delta_B.append(dB)
        delta_W.append(dW)
        
        for i in range(L-2)[::-1]:
            delta = np.dot(delta,self.weights[i+1].transpose())*sigmoid_derivative(sums[i])
            dB = delta
            dW = np.einsum('bi,bj->bij',activations[i],delta)
            delta_B.append(dB)
            delta_W.append(dW)
        return delta_W[::-1], delta_B[::-1]
    
    def make_batch(self, X, Y, batch_index, batch_size):
      
        def shuffle_data(X, Y):
            n = X.shape[1]
            data = np.concatenate([X, Y], axis=1)
            np.random.shuffle(data)
            return np.split(data, [n], axis=1)
        
        if batch_size >= X.shape[0]:
            return X, Y, 0
          
        elif batch_index+batch_size >= X.shape[0]:
            X, Y = shuffle_data(X, Y)
            return X[0:batch_size], Y[0:batch_size], 0
                    
        else:
            return X[batch_index:batch_index+batch_size], Y[batch_index:batch_index+batch_size], batch_index + batch_size
    
    def define_input_normalization(self,X_train):
        self.input_max = np.max(X_train,axis=0)
        self.input_min = np.min(X_train,axis=0) 
    def define_output_normalization(self,Y_train):
        self.output_max = np.max(Y_train,axis=0)
        self.output_min = np.min(Y_train,axis=0)     

    def normalize_input(self,x):
        return (x-self.input_min)/(self.input_max-self.input_min)
    def normalize_output(self,y):
        return (y-self.output_min)/(self.output_max-self.output_min)
    def denormalize_output(self,n):
        return n*(self.output_max-self.output_min)+self.output_min
    def train(self, X_train, Y_train, epochs, alpha = 1, batch_size = 16, val_batch_size = 32, cross_validation=0.8, verbosity=1):
    
        self.define_input_normalization(X_train)
        self.define_output_normalization(Y_train)
        X_train = self.normalize_input(X_train)
        Y_train = self.normalize_output(Y_train)

        n, p = X_train.shape[1], Y_train.shape[1]
        L = len(self.layers)
        
        # Preparation de la validation croisée et mélange des données
        data = np.concatenate([X_train, Y_train], axis=1)
        np.random.shuffle(data)
        data, val_data = np.split(data, [int(data.shape[0]*cross_validation)])
        
        x_train, y_train = np.split(data, [n], axis=1)
        x_val, y_val = np.split(data, [n], axis=1)
        
        batch_index, val_batch_index = 0, 0
        for epoch in range(epochs):
            # Preparation des batchs de manière efficace
            x_train_batch, y_train_batch, batch_index = self.make_batch(x_train, y_train, batch_index, batch_size)
            x_val_batch, y_val_batch, val_batch_index = self.make_batch(x_val, y_val, val_batch_index, val_batch_size)
            
            
            # Propagation arrière
            delta_W, delta_B = self.backpropagation(x_train_batch,y_train_batch)
               

            for i in range(L-1):
                # Mise à jour du réseau
                self.bias[i] -= alpha*np.mean(delta_B[i],axis=0)
                self.weights[i] -= alpha*np.mean(delta_W[i],axis=0)
            
            if verbosity == 1 and epoch%(epochs/10)==0:
                # Messages pour voir l'avancée
                cost = cost_function(self.prediction(x_train_batch),y_train_batch)
                val_cost = cost_function(self.prediction(x_val_batch),y_val_batch)
                print('Epoch {}/{} \t Cost: {}\t Val_cost: {}'.format(epoch,epochs,cost,val_cost))