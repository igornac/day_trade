import numpy as np

from automatants import MultiPerceptron
from biased_data import Biased
from yahoo import FinanceData, PERIOD_TYPE_YEAR, FREQUENCY_TYPE_HOUR

BIASED = Biased(input_size = 2,output_size = 1)
MSFT = FinanceData('MSFT')
MSFT.get_data(PERIOD_TYPE_YEAR,1,FREQUENCY_TYPE_HOUR,1)

# Neural Network for the mean
(x_train, y_train),(x_test,y_test) = BIASED.load_data(size = 1000,noise = 0)#MSFT.load_data()

MP1 = MultiPerceptron(2,1,[4,4,4,2])
MP1.train(x_train,y_train,1000,batch_size = 32,alpha = 0.5)
print('Standard deviation of the mean: ',np.round(np.sqrt(np.sum(np.square(MP1.prediction(x_test)-y_test))),6))

# Neural Network for the standard deviation
y_train = np.square(MP1.prediction(x_train)-y_train)
y_test_ini = y_test
y_test = np.square(MP1.prediction(x_test)-y_test)

MP2 = MultiPerceptron(2,1,[4,4,4,2])
MP2.train(x_train,y_train,1000,batch_size = 32,alpha = 0.5)
print('Standard deviation of the standard deviation: ',np.round(np.sqrt(np.sum(np.square(MP2.prediction(x_test)-y_test))),6))

#some results
for i in range(10):
    print('test',i)
    print('relative variation 1/3-2/3 = ',MP1.normalize_output(y_test_ini[i][0]))
    #print(x_test[i])
    #print(MP1.normalize(x_test[i]))
    #print(MP1.prediction(x_test[i]))
    #print(MP2.prediction(x_test[i]))
    print('predicted = ',MP1.normalize_output(MP1.prediction(x_test[i])[0]))
    print('predict interval (+- 1 sigma) = [',MP1.normalize_output(MP1.prediction(x_test[i])[0]-np.sqrt(MP2.prediction(x_test[i])[0])),',',MP1.normalize_output(MP1.prediction(x_test[i])[0]+np.sqrt(MP2.prediction(x_test[i])[0])) ,']')
